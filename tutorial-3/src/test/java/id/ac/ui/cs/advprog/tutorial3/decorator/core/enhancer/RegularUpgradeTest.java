package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Sword;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegularUpgradeTest {

    private RegularUpgrade regularUpgrade;

    @BeforeEach
    public void setUp(){
        regularUpgrade = new RegularUpgrade(new Sword());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String weaponName = regularUpgrade.weapon.getName();
        assertEquals("Sword", weaponName);

    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String weaponDesc = regularUpgrade.getDescription();
        assertEquals("Regular Great Sword", weaponDesc);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int weaponValue = regularUpgrade.getWeaponValue();
        assertTrue(weaponValue > regularUpgrade.weapon.getWeaponValue());
        assertEquals(25, regularUpgrade.weapon.getWeaponValue());
    }
}
