package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RawUpgradeTest {

    private RawUpgrade rawUpgrade;

    @BeforeEach
    public void setUp(){
        rawUpgrade = new RawUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String weaponName = rawUpgrade.weapon.getName();
        assertEquals("Shield", weaponName);
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String weaponDesc = rawUpgrade.getDescription();
        assertEquals("Raw Heater Shield", weaponDesc);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int weaponValue = rawUpgrade.getWeaponValue();
        assertTrue(weaponValue > rawUpgrade.weapon.getWeaponValue());
        assertEquals(10, rawUpgrade.weapon.getWeaponValue());
    }
}
