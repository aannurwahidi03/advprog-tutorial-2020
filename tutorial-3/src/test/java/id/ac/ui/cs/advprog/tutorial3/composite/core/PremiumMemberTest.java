package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        String nama = member.getName();
        assertEquals("Wati", nama);
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        String role = member.getRole();
        assertEquals("Gold Merchant", role);
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member thisMember = new PremiumMember("Test", "Merchant");
        member.addChildMember(thisMember);
        assertEquals(thisMember, member.getChildMembers().get(0));
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member test = new PremiumMember("test", "Merchant");
        member.addChildMember(test);
        member.removeChildMember(test);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member test = new PremiumMember("test", "Merchant");
        member.addChildMember(test);

        Member test1 = new PremiumMember("test1", "Singer");
        member.addChildMember(test1);

        Member test2 = new PremiumMember("Haji", "Ustadz");
        member.addChildMember(test2);

        Member test3 = new PremiumMember("Nani", "Tukang Bakso");
        member.addChildMember(test3);

        assertEquals(3, member.getChildMembers().size());

    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member master = new PremiumMember("Mastah", "Master");
        for(int i = 0; i < 5; i++) {
            Member member1 = new OrdinaryMember("Member" + i, "Member");
            master.addChildMember(member1);
        }

        assertEquals(5, master.getChildMembers().size());

    }
}
