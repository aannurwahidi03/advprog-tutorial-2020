package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        Member test = new PremiumMember("Aan", "Gold");
        guild.addMember(guildMaster, test);

        Member found = guild.getMember("Aan", "Gold");
        assertNotNull(found);
        assertEquals(found.getName(), "Aan");
        assertEquals(found.getRole(), "Gold");
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        Member tust = new PremiumMember("Tust", "Merchant");
        guild.addMember(guildMaster, tust);
        Member test = new PremiumMember("Aan", "Gold");
        guild.addMember(tust, test);
        guild.removeMember(tust, test);
        assertEquals(0, tust.getChildMembers().size());
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member thisMember = new PremiumMember("Goku", "SaiyaJin");
        guild.addMember(guildMaster, thisMember);
        assertEquals(thisMember, guild.getMember("Goku", "SaiyaJin"));
    }
}
