package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MagicUpgradeTest {


    private MagicUpgrade magicUpgrade;

    @BeforeEach
    public void setUp(){
        magicUpgrade = new MagicUpgrade(new Longbow());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String weaponName = magicUpgrade.weapon.getName();
        assertEquals("LongBow", weaponName);
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String weaponDesc = magicUpgrade.getDescription();
//        System.out.println(weaponDesc);
        assertEquals("Magic Big LongBow", weaponDesc);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int weaponValue = magicUpgrade.getWeaponValue();
        assertTrue(weaponValue > magicUpgrade.weapon.getWeaponValue());
        assertEquals(15, magicUpgrade.weapon.getWeaponValue());
    }
}
