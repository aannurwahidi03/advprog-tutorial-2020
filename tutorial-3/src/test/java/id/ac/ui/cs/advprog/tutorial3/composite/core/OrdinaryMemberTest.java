package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Nina", "Merchant");
    }

    @Test
    public void testMethodGetName() {
        //TODO: Complete me
        String name = member.getName();
        assertEquals(name,"Nina");
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        String role = member.getRole();
        assertEquals(role, "Merchant");
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        //TODO: Complete me
        Member ini = new OrdinaryMember("Test", "Gold Merchant");
        member.addChildMember(ini);
        assertEquals(0, member.getChildMembers().size());

    }
}
