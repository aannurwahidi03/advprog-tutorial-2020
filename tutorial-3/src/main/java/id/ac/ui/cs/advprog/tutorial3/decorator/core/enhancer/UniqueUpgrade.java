package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;
    Random random;

    public UniqueUpgrade(Weapon weapon){

        this.weapon = weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        random = new Random();
        int upgrade = (random.nextInt((15-10)+1) + 10) + weapon.getWeaponValue();
        return upgrade;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Unique " + weapon.getDescription();
    }
}
