package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class MagicUpgrade extends Weapon {

    Weapon weapon;
    Random random;

    public MagicUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 15-20 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        random = new Random();
        int upgrade = (random.nextInt((20-15)+1) + 15) + weapon.getWeaponValue();
        return upgrade;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Magic " + weapon.getDescription();
    }
}
