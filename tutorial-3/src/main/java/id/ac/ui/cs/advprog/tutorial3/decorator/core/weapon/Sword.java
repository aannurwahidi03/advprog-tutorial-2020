package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {
        //TODO: Complete me
    public Sword() {
        this.weaponValue = 25;
        this.weaponName = "Sword";
        this.weaponDescription = "Great Sword";
    }
}
