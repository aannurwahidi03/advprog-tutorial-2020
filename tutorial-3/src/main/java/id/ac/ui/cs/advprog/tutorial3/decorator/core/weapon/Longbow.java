package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
        //TODO: Complete me
    public Longbow() {
        this.weaponValue = 15;
        this.weaponName = "LongBow";
        this.weaponDescription = "Big LongBow";
    }
}
