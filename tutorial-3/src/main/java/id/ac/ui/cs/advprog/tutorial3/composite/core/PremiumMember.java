package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
        //TODO: Complete me
    public List<Member> memberList;
    String name;
    String role;
    public PremiumMember(String name, String role) {
        this.name = name;
        this.role = role;
        memberList = new ArrayList<>();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return this.role;
    }

    @Override
    public void addChildMember(Member member) {
        if(memberList.size() < 3 || role.compareTo("Master") == 0)
            this.memberList.add(member);
    }

    @Override
    public void removeChildMember(Member member) {
        memberList.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return memberList;
    }
}
