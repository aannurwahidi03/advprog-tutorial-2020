package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;
    Random random;

    public RawUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        random = new Random();
        int upgrade = (random.nextInt((10-5)+1) + 5) + weapon.getWeaponValue();
        return upgrade;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Raw " + weapon.getDescription();
    }
}
