package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {

        public KnightAdventurer(Guild guild) {
                this.name = "Knight";
                this.guild = guild;
                this.guild.add(this);
                //ToDo: Complete Me
        }

        @Override
        public void update() {
                String type = guild.getQuestType();
                if(type.equals("Rumble") || type.equals("Delivery") || type.equals("Escort")) {
                        this.getQuests().add(guild.getQuest());
                }
        }

        //ToDo: Complete Me
}
