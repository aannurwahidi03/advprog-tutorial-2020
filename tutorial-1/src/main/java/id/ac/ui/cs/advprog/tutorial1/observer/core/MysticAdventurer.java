package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild = guild;
                this.guild.add(this);
                //ToDo: Complete Me
        }

        @Override
        public void update() {
                String type = guild.getQuestType();
                if(type.equals("Delivery") || type.equals("Escort")) {
                        this.getQuests().add(guild.getQuest());
                }
        }
}
