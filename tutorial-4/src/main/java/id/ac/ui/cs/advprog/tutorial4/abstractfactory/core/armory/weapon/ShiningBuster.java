package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon;

public class ShiningBuster implements Weapon {
    String name;
    String description;

    public ShiningBuster() {
        this.name = "Shining Buster";
        this.description = "Holy attack that delivers damage with 6 powerfull attack";
    }

    @Override
    public String getName() {
        // TODO fix me
        return this.name;
    }

    @Override
    public String getDescription() {
        // TODO fix me
        return this.description;
    }
}
