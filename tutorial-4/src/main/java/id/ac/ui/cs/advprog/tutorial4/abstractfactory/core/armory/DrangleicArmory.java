package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.MetalArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ThousandYearsOfPain;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ThousandJacker;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;

public class DrangleicArmory implements Armory {
    MetalArmor armor;
    ThousandJacker jack;
    ThousandYearsOfPain pain;

    @Override
    public Armor craftArmor() {
        // TODO fix me
        armor = new MetalArmor();
        return armor;
    }

    @Override
    public Weapon craftWeapon() {
        // TODO fix me
        jack = new ThousandJacker();
        return jack;
    }

    @Override
    public Skill learnSkill() {
        // TODO fix me
        pain = new ThousandYearsOfPain();
        return pain;
    }
}
