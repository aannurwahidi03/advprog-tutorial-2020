package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor;

public class ShiningArmor implements Armor {
    String name;
    String description;

    public ShiningArmor() {
        this.name = "Shining Armor";
        this.description = "Armor created by the God of Sun";
    }

    @Override
    public String getName() {
        // TODO fix me
        return this.name;
    }

    @Override
    public String getDescription() {
        // TODO fix me
        return this.description;
    }
}
