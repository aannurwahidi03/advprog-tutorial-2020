package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill;

import sun.awt.SunHints;

public class ShiningForce implements Skill {
    String name;
    String description;
    public ShiningForce() {
        this.name = "Shining Force";
        this.description = "The Power of The Sun";
    }

    @Override
    public String getName() {
        // TODO fix me
        return this.name;
    }

    @Override
    public String getDescription() {
        // TODO fix me
        return this.description;
    }
}
