package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.LordranArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

public class LordranAcademy extends KnightAcademy {
    String name;

    public LordranAcademy() {
        this.name = "Lordran";
    }

    @Override
    public String getName() {
        // TODO complete me
        return this.name;
    }

    @Override
    public Knight produceKnight(String type) {
        Knight knight = null;
        Armory armory = new LordranArmory();

        switch (type) {
            case "majestic":
                knight = new MajesticKnight(armory);
                // TODO complete me

                break;
            case "metal cluster":
                knight = new MetalClusterKnight(armory);
                // TODO complete me

                break;
            case "synthetic":
                knight = new SyntheticKnight(armory);
                // TODO complete me

                break;
        }

        return knight;
    }
}
