package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.ShiningArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ShiningForce;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ShiningBuster;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;

public class LordranArmory implements Armory {
    ShiningBuster buster;
    ShiningArmor armor;
    ShiningForce force;

    @Override
    public Armor craftArmor() {
        // TODO fix me
        armor = new ShiningArmor();
        return armor;
    }

    @Override
    public Weapon craftWeapon() {
        // TODO fix me
        buster = new ShiningBuster();
        return buster;
    }

    @Override
    public Skill learnSkill() {
        // TODO fix me
        force = new ShiningForce();
        return force;
    }
}
