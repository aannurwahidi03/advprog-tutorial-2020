package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor;

public class MetalArmor implements Armor {
    String name;
    String description;

    public MetalArmor() {
        this.name = "Metal Armor";
        this.description = "Armor with good Defense";
    }

    @Override
    public String getName() {
        // TODO fix me
        return this.name;
    }

    @Override
    public String getDescription() {
        // TODO fix me
        return this.description;
    }
}
