package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon;

public class ThousandJacker implements Weapon {
    String name;
    String description;

    public ThousandJacker() {
        this.name = "Thousand Jacker";
        this.description = "Powerfull attack with thousand of arrows";
    }

    @Override
    public String getName() {
        // TODO fix me
        return this.name;
    }

    @Override
    public String getDescription() {
        // TODO fix me
        return this.description;
    }
}
