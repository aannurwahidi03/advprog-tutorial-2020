package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill;

public class ThousandYearsOfPain implements Skill {
    String name;
    String description;

    public ThousandYearsOfPain() {
        this.name = "Thousand Years of Pain";
        this.description = "Drops Big Meteorites from Space";
    }

    @Override
    public String getName() {
        // TODO fix me
        return this.name;
    }

    @Override
    public String getDescription() {
        // TODO fix me
        return this.description;
    }
}
