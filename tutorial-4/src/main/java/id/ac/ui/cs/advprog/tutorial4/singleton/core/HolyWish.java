package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {

    private String wish;
    public static final HolyWish INSTANCE = new HolyWish();

    // TODO complete me with any Singleton approach
    private HolyWish() {
        this.wish = "Jadi Kaya";
    }

    public static HolyWish getInstance() {
        return INSTANCE;
    }

    public String getWish() {
        return this.wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return this.wish;
    }
}
