package id.ac.ui.cs.advprog.tutorial4.abstractfactory.armory.skill;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ThousandYearsOfPain;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ThousandYearsOfPainTest {

    Skill thousandYearsOfPain;

    @BeforeEach
    public void setUp(){
        thousandYearsOfPain = new ThousandYearsOfPain();
    }

    @Test
    public void testToString(){
       // TODO create test
        String thisName = thousandYearsOfPain.getName();
        assertEquals(thisName, "Thousand Years of Pain");
    }

    @Test
    public void testDescription(){
        // TODO create test
        String thisDesc = thousandYearsOfPain.getDescription();
        assertEquals(thisDesc, "Drops Big Meteorites from Space");
    }
}
