package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
//import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import java.util.List;
import java.util.ArrayList;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    // TODO create tests
    @Test
    public void whenGetKnightAcademiesItShouldCallAcademyRepositoryGetKnightAcademies() {
        academyService.getKnightAcademies();

        verify(academyRepository, times(1)).getKnightAcademies();
    }

    @Test
    public void whenProduceKnightItShouldCallAcademicRepository() {
        academyService = new AcademyServiceImpl(academyRepository);
        academyService.seed();
        when(academyRepository.getKnightAcademyByName("Drangleic")).thenReturn(new DrangleicAcademy());

        academyService.produceKnight("Drangleic", "majestic");
        List<KnightAcademy> list = new ArrayList<>();
        list.add(new DrangleicAcademy());
        when(academyRepository.getKnightAcademies()).thenReturn(list);

        assertEquals("Drangleic", academyService.getKnightAcademies().get(0).getName());
        assertNotNull(academyService.getKnight());
    }


}
