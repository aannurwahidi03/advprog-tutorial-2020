package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {
    // TODO create tests

    @Test
    public void whenHolyGrailCallMakeAWishItShouldCallInstanceAndSetWish() {
        HolyGrail holyGrail = spy(new HolyGrail());
        String wish = "Test";

        doCallRealMethod().when(holyGrail).makeAWish(wish);
        holyGrail.makeAWish(wish);

        verify(holyGrail).makeAWish(wish);
    }

    @Test
    public void whenGetHolyWishItShouldReturnThisHolyWish() {
        HolyGrail spyHoly = spy(new HolyGrail());

        doCallRealMethod().when(spyHoly).getHolyWish();
        spyHoly.getHolyWish();

        verify(spyHoly).getHolyWish().getInstance();
    }

}
