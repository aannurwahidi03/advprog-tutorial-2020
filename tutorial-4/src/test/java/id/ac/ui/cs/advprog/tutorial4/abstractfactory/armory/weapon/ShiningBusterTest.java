package id.ac.ui.cs.advprog.tutorial4.abstractfactory.armory.weapon;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ShiningBuster;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ShiningBusterTest {

    Weapon shiningBuster;

    @BeforeEach
    public void setUp(){
        shiningBuster = new ShiningBuster();
    }

    @Test
    public void testToString(){
        // TODO create test
        String thisName = shiningBuster.getName();
        assertEquals(thisName, "Shining Buster");
    }

    @Test
    public void testDescription(){
        // TODO create test
        String thisDesc = shiningBuster.getDescription();
        assertEquals(thisDesc, "Holy attack that delivers damage with 6 powerfull attack");
    }
}
