package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {

    @Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        if (rawAge<30) {
            return rawAge*2000;
        } else if (rawAge <50) {
            return rawAge*2250;
        } else {
            return rawAge*5000;
        }
    }

    @Override
    public String powerClassifier(int rawAge) {
        String powerClass = "";
        if (rawAge == 0 || rawAge < 20000) {
            powerClass = "C class";
        } else if (rawAge == 20000 || rawAge < 100000) {
            powerClass = "B class";
        } else if (rawAge >= 100000) {
            powerClass = "A class";
        }
        return powerClass;
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }
}
