package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpiritState;

public class HighSpiritSealSpell extends HighSpiritSpell {
    // TODO: Complete Me
    protected HighSpirit highSpirit;
    protected HighSpiritState highSpiritState;

    public HighSpiritSealSpell(HighSpirit highSpirit) {
        super(highSpirit);
    }


    @Override
    public void cast() {
        spirit.seal();
    }

    @Override
    public String spellName() {
        return spirit.getRace() + ":Seal";
    }
}
